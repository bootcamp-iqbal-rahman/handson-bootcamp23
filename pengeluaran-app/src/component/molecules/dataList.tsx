import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux/reducers';
import { Table } from '../atom/table';

const DataList: React.FC = () => {
    const expenses = useSelector((state: RootState) => state.expenses);

    return (
        <ul className='mt-10 shadow-lg'>
            <Table 
                value={expenses}
            />
        </ul>
    );
};

export default DataList;
