import { useState } from "react";
import { Input } from "../atom/input";
import { Button } from "../atom/button";
import { useDispatch } from "react-redux";
import { addExpense } from "../../redux/actions/ExpensesAction";
import { v4 } from 'uuid';

export const FormInput: React.FC = () => {
    const [description, setDesc] = useState("");
    const [amount, setAmount] = useState("");
    const dispatch = useDispatch();

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        const expense: Expense = {
            id: v4(),
            description: description,
            amount: parseFloat(amount),
            date: Date.now(),
        };
        dispatch(addExpense(expense));
        setDesc('');
        setAmount('');
    };

    return (
        <div className="justify-center bg-slate-100 rounded-lg px-10 py-6 shadow-lg sm:mx-16 lg:mx-96">
            <form
                onSubmit={handleSubmit}>
                <Input 
                    id={"description"} 
                    text="Description"
                    type="text"
                    placeholder="Masukkan deskripsi pengeluaran"
                    value={description}
                    onChange={(e) => setDesc(e.target.value)} 
                />
                <Input 
                    id={"amount"} 
                    text="Besar pengeluaran"
                    type="text"
                    placeholder="Rp. "
                    value={amount}
                    onChange={(e) => setAmount(e.target.value)} 
                />
                <Button type={"submit"} text={"Tambah data"} />
            </form>
        </div>
    );
}