import React from "react";
import DataList from "../molecules/dataList";
import { FormInput } from "../molecules/formInput";

export const HomePage: React.FC = () => {
    return (
        <div>
            <FormInput />
            <DataList />
        </div>
    );
}