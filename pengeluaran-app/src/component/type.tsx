interface Expense {
    id: string;
    description: string;
    amount: number;
    date: number;
}

interface Action {
    type: string;
    payload: any;
}

interface ExpensesState {
    expenses: Expense[];
    totalExpense: number;
}

interface InputField {
    id: string;
    text: string;
    type: string;
    placeholder: string;
    value: string;
    onChange: (e :any) => void;
}

interface ButtonComponent {
    type: "button" | "submit" | "reset";
    text: string;
}

interface TableComponent {
    value: ExpensesState;
}