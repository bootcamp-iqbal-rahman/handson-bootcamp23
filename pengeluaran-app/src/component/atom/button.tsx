import React from "react";

export const Button: React.FC<ButtonComponent> = (props) => {
    return (
        <button
            type={props.type}
            className="button-form text-white font-black bg-slate-500 hover:bg-4 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full px-5 py-2.5 text-center"
        >{props.text}</button>
    );
}