import { useDispatch } from 'react-redux';
import { deleteExpense } from '../../redux/actions/ExpensesAction';
import { Modal } from './modal';

export const Table: React.FC<TableComponent> = (props) => {
    const dispatch = useDispatch();

    return (
        <>
            <div className="relative overflow-x-auto">
                <table className="w-full text-sm text-left rtl:text-right text-gray-500">
                    <thead className="text-xs text-gray-700 uppercase bg-gray-50">
                        <tr>
                            <th scope="col" className="px-6 py-3">
                                No
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Expense
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Amount
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Date
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.value.expenses.map((item, index) => (
                            <tr className="bg-white border-b" key={index}>
                                <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                    {index + 1}
                                </th>
                                <td className="px-6 py-4">
                                    {item.description}
                                </td>
                                <td className="px-6 py-4">
                                    Rp {item.amount}
                                </td>
                                <td className="px-6 py-4">
                                    {item.date}
                                </td>
                                <td className="px-6 py-4 space-x-3">
                                    <a
                                        data-modal-target="authentication-modal" data-modal-toggle="authentication-modal"
                                        className="bg-blue-500 text-white px-4 py-2 rounded">
                                            <Modal id={item.id} description={item.description} amount={item.amount} date={item.date} /></a>
                                    <button 
                                        onClick={() => dispatch(deleteExpense(item.id))}
                                        className="bg-red-500 text-white px-4 py-2 rounded">Delete</button>
                                </td>
                            </tr>
                        ))}
                        <tr>
                            <td></td>
                            <th className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                Total Amount
                            </th>
                            <td className="px-6 py-4">
                                Rp {props.value.totalExpense}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </>
    );
}