import React, { useState } from "react";
import {
    Button,
    Dialog,
    DialogHeader,
    DialogBody,
} from "@material-tailwind/react";
import { Input } from "./input";
import { useDispatch } from "react-redux";
import { editExpense } from "../../redux/actions/ExpensesAction";

export const Modal: React.FC<Expense> = (props) => {
    const [open, setOpen] = React.useState(false);
    const [newDesc, setDesc] = React.useState("");
    const [newAmount, setAmount] = React.useState("");
    const handleOpen = () => setOpen(!open);
    const dispatch = useDispatch();

    const handleSubmit = () => {
        dispatch(editExpense(props.id, { 
            description: newDesc, 
            amount: parseFloat(newAmount),
            date: props.date
        }));
    }

    useState(() => {
        setDesc(props.description);
        setAmount(props.amount.toString());
    })

    return (
        <>
            <button onClick={handleOpen}>
                Edit
            </button>
            <Dialog open={open} handler={handleOpen} placeholder={null}>
                <DialogHeader placeholder={"Edit Data"}>Edit data</DialogHeader>
                <DialogBody placeholder={"body"}>
                    <Input 
                        id={"new-description"} 
                        text={"Description"} 
                        type={"text"} 
                        placeholder={"Input description here"} 
                        value={newDesc} 
                        onChange={(e) => setDesc(e.target.value)} 
                    />
                    <Input 
                        id={"new-amount"} 
                        text={"Amount"} 
                        type={"text"} 
                        placeholder={"Input amount here"} 
                        value={newAmount} 
                        onChange={(e) => setAmount(e.target.value)} 
                    />
                </DialogBody>
                <Button variant="gradient" color="green" onClick={handleSubmit} placeholder={"submit"}>
                    <span>Update Data</span>
                </Button>
            </Dialog>
        </>
    );
}