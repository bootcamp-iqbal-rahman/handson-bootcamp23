import { combineReducers } from 'redux';
import expensesReducer from './ExpenseReducer';

const rootReducer = combineReducers({
    expenses: expensesReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
