import { ADD_EXPENSE, EDIT_EXPENSE, DELETE_EXPENSE } from '../actions/ExpensesAction';
import { ExpenseActionTypes } from '../actions/ExpensesAction'

const initialState: ExpensesState = {
    expenses: [],
    totalExpense: 0,
};

const expensesReducer = (state = initialState, action: ExpenseActionTypes): ExpensesState => {
    switch (action.type) {
        case ADD_EXPENSE:
            return {
                ...state,
                expenses: [...state.expenses, action.payload],
                totalExpense: state.totalExpense + action.payload.amount,
            };
        case EDIT_EXPENSE:
            return {
                ...state,
                expenses: state.expenses.map(expense =>
                    expense.id === action.payload.id ? { ...expense, ...action.payload.updates } : expense
                ),
            };
        case DELETE_EXPENSE:
            return {
                ...state,
                expenses: state.expenses.filter(expense => expense.id !== action.payload),
                totalExpense: state.expenses.reduce((acc, expense) => expense.id === action.payload ? acc - expense.amount : acc, state.totalExpense),
            };
        default:
            return state;
    }
};

export default expensesReducer;
