export const ADD_EXPENSE = 'ADD_EXPENSE';
export const EDIT_EXPENSE = 'EDIT_EXPENSE';
export const DELETE_EXPENSE = 'DELETE_EXPENSE';

export interface AddExpenseAction {
    type: typeof ADD_EXPENSE;
    payload: Expense;
}

export interface EditExpenseAction {
    type: typeof EDIT_EXPENSE;
    payload: { id: string; updates: Partial<Expense> };
}

export interface DeleteExpenseAction {
    type: typeof DELETE_EXPENSE;
    payload: string;
}

export type ExpenseActionTypes = AddExpenseAction | EditExpenseAction | DeleteExpenseAction;

export const addExpense = (expense: Expense) => ({
    type: ADD_EXPENSE,
    payload: expense,
});

export const editExpense = (id: string, updates: Partial<Expense>) => ({
    type: EDIT_EXPENSE,
    payload: { id, updates },
});

export const deleteExpense = (id: string) => ({
    type: DELETE_EXPENSE,
    payload: id,
});
